# RealMe, a Drupal 8 website providing a FAQ feature.

## Features provided
- A subtheme of Radix is used to provide a theme based on BootStrap4.
- A Drupal feature providing the FAQ Feature, including:
  - FAQ content type
    - With pathauto pattern /faq/topicName/nodeTitle
    - New view mode : accordion to keep teaser mode for front page and any other pages like Search results ?
  - Topic vocabulary
    - With pathauto pattern /faq/topicName
  - 3 views:
    - /faq, displays all faq
    - /taxonomy/term/%, displays faq from one topic
    - /admin/content/reorder-faq, allows to reorder faq
  - Custom navigation Block:
    - Visible on path /faq* (so also displayed on node page of content type FAQ).
    - If one topic is added or topics are reordered, the cache will be invalidated.
  - Content editor role
    - Can create/edit/delete any FAQ node and topic

## Requirements
- Docker
- Docker compose
- Composer
- npm or yarn if you want to improve theming

## Installation
- Clone the project
- Execute the command composer install, it will download Drupal core and contrib modules required by this website ( Devel and Features are only necessary in dev environment)
- Execute the command 'make' ( if you don't have Linux envrionment just use docker compose up) that will spin up the dev environment using docker and docker compose.(NGINX, Mariadb, PHP 7.4, PhpMyAdmin, Mailhog)
- The website will be available at the url http://realme.docker.localhost (should work without setting anything in your host file)
- Start the usual Drupal installation as usual. **The development has been only tested with a LTR language.**
  - db name: realme ( you can change all db details in .env file)
  - db root: root ( if you want to avoid to create db manually, but not recommended)
  - db root password: password
- Install the module Component Libraries, required by Radix
- Enable the subtheme **Real theme**
- Enable the feature **Real FAQ Feature**

## Add some dummy content
- Enable the module Devel Generate
- Generate some Taxonomy Terms from vocabulary Topic
- Generate some Nodes from content type FAQ
- Optionally you can generate Path Alias for all those nodes

## Dump
- If you use the dump.sql file.
- The dummy Content is already generated
- 2 users are avilable:
  - Admin user: admin / password: 123
  - Editor user: editor1 / pasword: 123
- Modules Devel generate, Features and features are enabled

## Improve design
- If you want to change the CSS, execute the command 'npm install' or yarn in the subtheme directory web/themes/custom/real_theme, it will download all required libraries by the theme BootStrap, Bootswatch, Jquery and nice tools like Sass compiler, Browser-Sync, webpack...
- If you want to change the markup of the navigation block, copy the yml file web/modules/custom/real_faq_feature/templates/real_faq-nav-faqs.html.twig inside the templates directory of the subtheme.
- You can recompile BootStrap with different sass variable, remove unused components...

