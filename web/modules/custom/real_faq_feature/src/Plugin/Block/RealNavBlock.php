<?php

namespace Drupal\real_faq_feature\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Url;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Routing\CurrentRouteMatch;

/**
 * Provides a 'RealFaqBlock' block.
 *
 * @Block(
 *  id = "real_faq_nav_block",
 *  admin_label = @Translation("Real FAQ Nav"),
 * )
 */
class RealNavBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $request;

  /**
   * {@inheritdoc}.
   */
  public function __construct(array $configuration,
                              $plugin_id,
                              $plugin_definition,
                              EntityTypeManagerInterface $entityTypeManager,
                              currentRouteMatch $currentRouteMatch) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entityTypeManager;
    $this->currentRouteMatch = $currentRouteMatch;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('current_route_match'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $entity_storage = $this->entityTypeManager->getStorage('taxonomy_term');
    $query_result = $entity_storage->getQuery()
      ->condition('vid', 'topic')
      ->sort('weight', 'ASC')
      ->execute();
    $topics = $entity_storage->loadMultiple($query_result);

    $topics_items = [];
    $topics_items[] = [
      'url' => Url::fromRoute('view.faqs.page'),
      'name' => $this->t('All'),
      'active' => $this->currentRouteMatch->getRouteName() === 'view.faqs.page' ? TRUE : FALSE
    ];
    $term = $this->currentRouteMatch->getParameter('taxonomy_term');
    foreach ($topics as $topic) {
      $url = $topic->toUrl()->toString();
      $active = $term ? ($term->id() === $topic->id() ? TRUE : FALSE) : FALSE;
      $rendered_entity = $this->entityTypeManager->getViewBuilder('taxonomy_term')->view($topic, 'teaser');
      $topics_items[] = [
        'url' => $url,
        'name' => $topic->label(),
        'active' => $active,
        'teaser' => $rendered_entity
      ];
    }

    $topics_list =  [
      '#theme' => 'nav_faqs',
      '#topics' => $topics_items,
    ];

    return [
      'type_list' => $topics_list,
      '#cache' => [
        'keys' => ['real:faq_topics'],
        'contexts' => ['url'],
        'tags' => ['taxonomy_term_list:topic'],
      ],
    ];
  }

}
