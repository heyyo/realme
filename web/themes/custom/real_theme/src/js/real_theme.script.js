import 'popper.js';
import 'bootstrap';

(function ($, Drupal) {

  'use strict';
  if ($('#nav-topics').length > 0) {
    $('#nav-topics .nav-link.active').on('click', (e) => {
      e.preventDefault();
      $('.block--real-theme-content').toggle();
    })
  }

})(jQuery, Drupal);